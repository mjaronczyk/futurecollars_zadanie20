from flask import Flask
from flask import Blueprint
from modules.shared import db
from modules.options import options_blueprint
from flask_migrate import Migrate

app = Flask(__name__)

app.register_blueprint(options_blueprint)

app.config['SECRET_KEY'] = '4315432523523532453245324523452345vfdsgfdsgsdgsdfg'


db.init_app(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///modules/database.db'




migrate = Migrate(app, db)

app.run(debug = True)