from atexit import register
from flask import Blueprint, render_template, request
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired
from modules.accountant import Accountant
from jinja2 import Template
from modules.shared import db
from modules.db_alchemy import History, Assortment, Balance



options_blueprint = Blueprint('options', __name__)



# @options_blueprint.route('/create_base')
# def create_table():
#     db.create_all()
#     return "Zrobiono!"



@options_blueprint.route('/')
def home():
    balance = Accountant().show_balance()
    assortment = Accountant().show_assortment()
    
    kwota_saldo = request.args.get("cena_saldo")
    kwota_zakup = request.args.get("cena_zakup")
    kwota_sprzedaz = request.args.get("cena_sprzedaz")
    id_produktu = request.args.get("id")
    qty = request.args.get("ilosc")
    komentarz = request.args.get("komentarz")
    
    

    if komentarz:
        entry = "saldo " + komentarz
        Accountant().add_history(entry, kwota_saldo)
        Accountant().update_balance(kwota_saldo)
        pass

    elif kwota_zakup:
        Accountant().buy(id_produktu, kwota_zakup, qty)
    
    elif kwota_sprzedaz:
        Accountant().sell(id_produktu, kwota_sprzedaz, qty)
        
    print(Accountant().show_assortment())
    return render_template("home.html", balance=balance, assortment=assortment)




@options_blueprint.route('/saldo')
def saldo():

    return render_template("saldo.html")



@options_blueprint.route('/zakup')
def zakup():
    return render_template("zakup.html")



@options_blueprint.route('/sprzedaz')
def sprzedaz():
    return render_template("sprzedaz.html")


@options_blueprint.route('/historia')
@options_blueprint.route('/historia/<page1>')
@options_blueprint.route('/historia/<page1>/<page2>')
def historia(page1=None, page2=None):


    history = Accountant().show_history()
    
    if page1 and page2:
        history = history[int(page1):int(page2)]
    elif page1 and not page2:
        history = history[int(page1):]
    else:
        history
    return render_template("historia.html", history=history, page1=page1, page2=page2)

