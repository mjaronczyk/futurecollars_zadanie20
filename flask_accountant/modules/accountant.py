from turtle import update
from modules.shared import db
from modules.db_alchemy import History, Assortment, Balance


class Accountant:


    def add_history(self, entry, sum):
        entry = History(entry=entry, sum=sum)
        db.session.add(entry)
        db.session.commit()
        


    def add_assortment(self, item, quantity):
        entry = Assortment(item=item, quantity=quantity)
        db.session.add(entry)
        db.session.commit()


    def update_balance(self, amount):
        entry = Balance(sum=amount)
        db.session.add(entry)
        db.session.commit()

    


    def show_history(self):
        show = History.query.all()
        print(show)
        return show

    
    def show_assortment(self):
        items_dict = {}
        items = Assortment.query.all()
        for element in items:
            try:
                items_dict[element.item] += element.quantity
            except:
                items_dict[element.item] = element.quantity
        return items_dict




    def show_balance(self):
        actual_saldo = 0
        entry = Balance.query.all()
        for i in entry:
            actual_saldo += i.sum
        return actual_saldo
    

    def buy(self, item, cost_of_1_item, qty):
        cost_of_1_item = int(cost_of_1_item)
        qty = int(qty)
        if int(cost_of_1_item) * int(qty) < int(self.show_balance()):
            self.add_assortment(item, qty)
            self.update_balance(-cost_of_1_item * qty)
            komentarz = "bought " + str(qty) + " "+ item
            self.add_history(komentarz, cost_of_1_item * qty)
        
    
    def sell(self, item, cost_of_1_item, qty):
        cost_of_1_item = int(cost_of_1_item)
        qty = int(qty)
        try:
            if qty <= int(self.show_assortment()[item]):
                self.add_assortment(item, -qty)
                self.update_balance(cost_of_1_item * qty)
                komentarz = "sold " + str(qty) + " "+ item
                self.add_history(komentarz, cost_of_1_item * qty)
        except:
            print("item nie istnieje w assortymencie")


        




    




