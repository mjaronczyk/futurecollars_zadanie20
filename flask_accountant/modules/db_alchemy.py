from flask_sqlalchemy import SQLAlchemy
from modules.shared import db





class History(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sum = db.Column(db.Integer, unique=False, nullable=False)
    entry = db.Column(db.String(400), unique=False, nullable=False)

class Assortment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    item = db.Column(db.String(400), unique=False, nullable=False)
    quantity = db.Column(db.Integer, unique=False, nullable=False)

class Balance(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sum = db.Column(db.Integer, unique=False, nullable=False)